select c.name,count(distinct ewc.employee_id) as employeeCount

from country c



join working_center wc on c.id = wc.country_id
join employee_working_center ewc on wc.id = ewc.working_center_id
join employee e on c.id = e.country_id

where exists(select e.id from employee e where e.id = ewc.employee_id and cast(DATEDIFF(curdate(), ewc.start_date) * 0.002738 as SIGNED) > 1)



group by c.name

order by employeeCount desc,c.name