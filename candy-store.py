def buying_candy( amount_of_money ) :
  if amount_of_money < 2 and amount_of_money > 0:
    return 1

  elif amount_of_money <= 0:
        return 0
  
  dp = {
    0: 1,
    1: 1
  }
  x = 1
  while x < amount_of_money:
    dp[x+1] = dp[x] + dp[x - 1]
    x+=1
	
  return dp[amount_of_money]
