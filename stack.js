export function stack(stackOperation, stackValue) {
	var stackHolder = {
	lastIndex: 3,
	storage : [
	  1,
	  '{id: 1,value: "obj"}',
	  "stringHolder",
	  46
	]
  };

  
  var push = function(value) {
	stackHolder.storage[stackHolder.lastIndex + 1] = value;
	stackHolder.lastIndex++;
	return stackHolder.storage;
  }
  
  var pop = function() {
    if (stackHolder.lastIndex === 0) {
	  return [];
    }
	
    var poppedItem = stackHolder.storage[stackHolder.lastIndex];
    delete stackHolder.storage[stackHolder.lastIndex];
	stackHolder.lastIndex--;
    return poppedItem;
  }
  
  var peek = function() {
	
	return stackHolder.storage[stackHolder.lastIndex];

  }
  
  var swap = function() {

	var temp = stackHolder.storage[stackHolder.lastIndex];
	stackHolder.storage[stackHolder.lastIndex] = stackHolder.storage[stackHolder.lastIndex - 1];
	stackHolder.storage[stackHolder.lastIndex - 1] = temp;

	return stackHolder.storage;
  }
  
  switch(stackOperation) {
	case 'push':
	  return push(stackValue);
	break;
	case 'pop':
	  return pop();
	break;
	case 'swap':
	  return swap();
	break;
	case 'peek':
	  return peek();
	break;
	default:
	  return stackHolder.storage;
  }
  
}